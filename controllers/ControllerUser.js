const connect  = require('../connect.js')

module.exports.departament = async function(req, res, next) {
	try{
		let conection = await connect
		let sql = await conection.query('SELECT * FROM departamento')
		res.status(200).json(sql.recordset)
	}catch(error){
		res.status(500).json({error : error.originalError})
	}
} 

module.exports.getUsers = async function(req, res, next) {
	try{
		let conection = await connect
		let sql = await conection.query('SELECT * FROM usuarios') // conection.query`SELECT * FROM usuarios WHERE nombre = ${value}`
		res.status(200).json(sql.recordset)
	}catch(error){
		res.status(500).json({error : error.originalError})
	}
}

module.exports.getIdUsers = async function(req, res, next){
	try{
		let { id } = req.params
		let conection = await connect
		let sql = await conection.query`SELECT * FROM usuarios WHERE id = ${id}`
		res.status(200).json(sql.recordset)
	}catch(error){
		res.status(500).json({error : error.originalError})
	}
}