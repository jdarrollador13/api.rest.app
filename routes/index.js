var express = require('express');
var router = express.Router();
const connect  = require('../connect.js')
const { departament, getUsers, getIdUsers } = require('../controllers/ControllerUser')

/* GET home page. */
router.get('/', departament )
router.get('/users/getusers', getUsers)
router.get('/users/:id', getIdUsers )

module.exports = router;
